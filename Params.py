""" Este script cumple con dos tareas específicas:
1 - Seleccionar los puntos de unidades geológicas especificas, usando
para ello un filtro por cantidad mínima de puntos sobre una unidad geológica (polígono)
2 - Identificación de valores atípicos; para ello se implementa una detección de anómalos
usando rangos intercuartiles para los valores de cada unidad geológica.

El resultado es un shapefile con los puntos identificados se acuerdo a su
comportamiento estadístico, siendo esta una ayuda para el proceso de interpolación
"""
#
# Filtrado de datos por unidad
#
#
Params = puntos[::] # Evita que Paramas tenga el mismo id de puntos
Params = Params.dropna()
# Espacializar información
Params = gp.GeoDataFrame(Params,geometry=gp.points_from_xy(Params.E_PTO,Params.N_PTO),crs=sistemas_crs[2])
Params = Params.to_crs(4326)

# Actualizar puntos con unidad geológica
Params = Params.sjoin(Uc) # La unidades geológicas han sido cargadas prevoamente en el modulo Nivs.py
Params = pd.DataFrame(Params.drop(columns=['geometry']))
# Filtrar los unidades geológicas con mínimo 6 puntos
print(f"Total puntos por unidad geológica \n {Params.groupby([Params.iloc[:,len(Params.columns)-1]]).size()}")
Uc_Puntos = Params.groupby([Params.iloc[:,len(Params.columns)-1]]).size()
Puntos_Min = 6 # La idea es que esta variable sea introducida
Uc_Puntos = Uc_Puntos [Uc_Puntos >=Puntos_Min]
# Seleccionar datos del conjunto total de datos

datos = pd.DataFrame(columns = list(Params.columns))
"""Antes de iniciar el bucle for es necesario
reiniciar el número de fila o index, pues este debe ser consecutivo 
iniciando en 0"""
Params = Params.reset_index()
for i in range(len(Uc_Puntos.index)):
    print(f"Buscando datos contenidos en la unidad {Uc_Puntos.index[i]}")
    for fila in range(len(Params)):
        if Params['COD'][fila] == Uc_Puntos.index[i]:
            datos.loc[fila] = Params.iloc[fila,:]
Params = datos
del datos
#
# Identificar valores atípicos usando logaritmo base 10
#
Params = Params.reset_index()
Column = list(Params.columns[5:11]) # Parámetros

# #Crear columnas de outliers utilizando valores normalizados
# for i in range(len(Column)):
#     Params[f'Outlier{Column[i]}'] = 'Normal'
# # Evaluar parámetros usando datos normalizados (log10)
# for i in range(len(Column)):
#     print(Column[i])
#     q1 = Params.groupby(['COD'])[Column[i]].apply(lambda x: np.percentile(np.log10(pd.to_numeric(x)),25))
#     q3 = Params.groupby(['COD'])[Column[i]].apply(lambda x: np.percentile(np.log10(pd.to_numeric(x)),75))
#     Mediana = Params.groupby(['COD'])[Column[i]].apply(lambda x: np.median(np.log10(pd.to_numeric(x))))
#     Minimo = Params.groupby(['COD'])[Column[i]].apply(lambda x: min(np.log10(pd.to_numeric(x))))
#     Maximo = Params.groupby(['COD'])[Column[i]].apply(lambda x: max(np.log10(pd.to_numeric(x))))
#     rango = q3 - q1
#     LimMin = q1 - 1.5 * rango
#     LimMax = q3 + 1.5 * rango
#     for fila in range(len(Params)):
#         if LimMax[Params['COD'].iloc[fila]] < np.log10(Params[Column[i]].iloc[fila]):
#             print(f'{Params[Column[i]].iloc[fila]} Es mayor que {10**(LimMax[Params["COD"].iloc[fila]])}')
#             Params[f'Outlier{Column[i]}'].iloc[fila] = 'Atípico'
#         if LimMin[Params['COD'].iloc[fila]] > np.log10(Params[Column[i]].iloc[fila]):
#             print(f'{Params[Column[i]].iloc[fila]} Es menor que {10**(LimMin[Params["COD"].iloc[fila]])}')
#             Params[f'Outlier{Column[i]}'].iloc[fila] = 'Atípico'

#
# Identificar valores atípicos usando rangos intercuartiles con valores originales
#
for i in range(len(Column)):
    Params[f'Outlier{Column[i]}'] = 'Normal'
# Evaluar parámetros usando datos normalizados (log10)
for i in range(len(Column)):
    print(Column[i])
    q1 = Params.groupby(['COD'])[Column[i]].apply(lambda x: np.percentile(pd.to_numeric(x),25))
    q3 = Params.groupby(['COD'])[Column[i]].apply(lambda x: np.percentile(pd.to_numeric(x),75))
    Mediana = Params.groupby(['COD'])[Column[i]].apply(lambda x: np.median(pd.to_numeric(x)))
    Minimo = Params.groupby(['COD'])[Column[i]].apply(lambda x: min(pd.to_numeric(x)))
    Maximo = Params.groupby(['COD'])[Column[i]].apply(lambda x: max(pd.to_numeric(x)))
    rango = q3 - q1
    LimMin = q1 - 1.5 * rango
    LimMax = q3 + 1.5 * rango
    for fila in range(len(Params)):
        if LimMax[Params['COD'].iloc[fila]] < Params[Column[i]].iloc[fila]:
            print(f'{Params[Column[i]].iloc[fila]} Es mayor que {LimMax[Params["COD"].iloc[fila]]}')
            Params[f'Outlier{Column[i]}'].iloc[fila] = 'Atípico'
        if LimMin[Params['COD'].iloc[fila]] > Params[Column[i]].iloc[fila]:
            print(f'{Params[Column[i]].iloc[fila]} Es menor que {LimMin[Params["COD"].iloc[fila]]}')
            Params[f'Outlier{Column[i]}'].iloc[fila] = 'Atípico'


# Graficas Parámetros en BoxPlot identificando outliers
ParPlot = Column + [Params.columns[17],
                    'T_PUNTO']+\
          list(Params.columns[18:25])#Se suma a los parámetros la columna de unidad geológica
ejes = {0:[0,0], # Creación de un diccionario para identificar variables y gráfico correspondiente
        1:[0,1],
        2:[0,2],
        3:[1,0],
        4:[1,1],
        5:[1,2]}
# Realizar boxplot para 6 parámetros
fig, axes = plt.subplots(2, 3) #Dos filas 3 columnas
fig.suptitle('Distribución de cuartiles por parámetro, tipo de punto y unidad geológica')
for variable, grafica in ejes.items():
    sbn.boxplot(ax=axes[grafica[0],grafica[1]], x=ParPlot[6], y=ParPlot[variable], hue=ParPlot[7], data=Params[ParPlot])
    sbn.stripplot(ax=axes[grafica[0],grafica[1]], x=ParPlot[6], y=ParPlot[variable], hue=ParPlot[7],
                  data=Params[ParPlot], jitter=True, dodge=True, marker='o', alpha=0.5, color='grey')
    axes[grafica[0],grafica[1]].set_title(f'Parámetro: {ParPlot[variable]}')

# Realizar grafico de cajas por unidad identificando outliers identificados
fig, axes = plt.subplots(2, 3) #Dos filas 3 columnas
fig.suptitle('Distribución de cuartiles por Unidad geológica: identificación de outliers')
for variable, grafica in ejes.items():
    sbn.boxplot(ax=axes[grafica[0],grafica[1]], x=ParPlot[6], y=ParPlot[variable], hue=ParPlot[6], data=Params[ParPlot])
    sbn.stripplot(ax=axes[grafica[0],grafica[1]], x=ParPlot[6], y=ParPlot[variable], hue=ParPlot[variable + 8],
                  data=Params[ParPlot], jitter=True, dodge=True, marker='o', alpha=0.5)
    axes[grafica[0],grafica[1]].set_title(f'Parámetro: {ParPlot[variable]}')
