Niveles = puntos[::] # Evita que Niveles  tenga el mismo id de puntos
# Reemplazar nan por valor para ingresar al if
Niveles['PROF_PZ'] = Niveles['PROF_PZ'].fillna('NA')
Niveles['PROF_AJ'] = Niveles['PROF_AJ'].fillna('NA')
Niveles['PROF_HUM'] = Niveles['PROF_HUM'].fillna('NA')

# Unificar en una sola columna información de profundidad

Niveles['Prof']='NA' # Iniciar profundidad en cero
for i in range(0,len(Niveles)):
    if Niveles['PROF_PZ'][i] != 'NA':
        Niveles['Prof'][i]=Niveles['PROF_PZ'][i]
    elif Niveles['PROF_AJ'][i] != 'NA':
        Niveles['Prof'][i]=Niveles['PROF_AJ'][i]

# Eliminar datos sin nivel
Niveles = Niveles[Niveles.PROF_HUM != 'NA']
# Eliminar datos sin dato de profundidad
Niveles = Niveles[Niveles.Prof != 'NA']

Niveles['Niveles'] = pd.to_numeric(Niveles['PROF_HUM'],errors='coerce')
Niveles['Niveles'] = Niveles.Niveles * -1

# Georreferenciar puntos
sistemas_crs = [3114,3115,3116,3117,3118]
Niveles = gp.GeoDataFrame(Niveles,geometry=gp.points_from_xy(Niveles.E_PTO,Niveles.N_PTO),
                          crs=sistemas_crs[2])
Niveles = Niveles.to_crs(4326)

# Leer DEM y adjuntar valores de altura
Dem12 = rasterio.open(f'{os.getcwd()}/Raster/DEM.tif')

x = rst.point_query(Niveles,f'{os.getcwd()}/Raster/DEM.tif')

Niveles['Altura'] = x

# Calcular Nivel menos topografía
Niveles['NivelTop'] = Niveles.Niveles + Niveles.Altura

# Obtener unidad geológica asociada
Uc = gp.read_file(os.getcwd() + '/SHP/UG_100K.shp')
Uc = Uc.to_crs(4326)
# Actualizar puntos con unidad geológica
Niveles=Niveles.sjoin(Uc)
# Imprimir resumen agrupado
print(Niveles.groupby([(Niveles.iloc[:,len(Niveles.columns)-1]),'T_NIVEL']).size())
