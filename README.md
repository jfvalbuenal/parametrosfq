**Nota: La información contenida en el repositorio no involucra los datos utilizados, para ejecutar estas tareas en un equipo local es necesario crear las carpetas `XLS`,`SHP` y `Raster` para almacenar allí la información temática a procesar y los datos de apoyo**

**Nota 2: Los procesos corren sobre `python 3.9`, las librerías utilizadas y sus versiones están nombradas en el archivo `requeriments.txt`**


# Evaluación estadística de inventario de puntos

El flujo de trabajo está compuesto por 3 módulos:

 - `datos.py`
 - `Nivs.py`
 - `Params.py`

siendo el primero de los listados el modulo que ejecuta los otros dos mencionados. El flujo de procesos está compuesto actualmente de 3 tareas.

## Lectura de datos
El modulo 1 lee los datos, almacenados en la carpeta `XLS`, allí se llama el documento `.xlsx` con la información revisada y estandarizada. El usuario debe indicar si se filtrará la información de acuerdo a su recolección en el periodo **seco** o **húmedo**. EL resultado de este proceso es una tabla con campos filtrado para procesarlo en las siguientes dos tareas.

## Niveles

Con el resultado del modulo uno se unifica en una sola columna la información existente de niveles, esto para los puntos de agua que así lo registra. Del mismo modo se unifica la información de profundidad en una sola columna.

A partir de la espacialización de la información se utiliza un *Modelo digital de elevación* para unificar la cota del punto sobre el terreno, con esta información se procede a calcular el nivel piezometrico general.
**Este proceso no diferencia por profundidad ni tipo de punto, es un proceso que se realiza para los datos que tienen información en los campos de interés**

## Identificación de outliers en parámetros físico - químicos
El ultimo paso ejecuta dos procesos concretos:

 - Seleccionar los puntos de unidades geológicas especificas, usando para ello un filtro por cantidad mínima de puntos sobre una unidad geológica (polígono)
 - Identificación de valores atípicos; para ello se implementa una detección de anómalos usando rangos intercuartiles para los valores, separando allí los parámetros identificados por unidad geológica.


