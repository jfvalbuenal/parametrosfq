# Limpiar directorio de trabajo
for name in dir():
    if not name.startswith('_'):
        del globals()[name]
#Librerias
import glob
import os
import geopandas as gp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pygeos
import rasterio
import rasterstats as rst
import seaborn as sbn
from matplotlib.tri import LinearTriInterpolator, Triangulation
from numpy.core.numeric import moveaxis
from numpy.lib.function_base import place
from numpy.lib.polynomial import polyfit
from scipy.interpolate import griddata
from shapely.geometry import Point
# Lee los inventarios que se gurden en la carpeta XLS
ruta = os.getcwd()+'/XLS/'
files = os.listdir(ruta)
# Lectura de datos
puntos = pd.DataFrame()
for i in glob.glob(f"{ruta}*.xlsx"):
    df = pd.read_excel(i)
    df = df.drop(range(0, 7)) # Eliminar filas sin información util
    puntos = puntos.append(df,ignore_index=True)

""" Los inventarios almacenan información temática
diferenciando el régimen de lluvias presente al momento de 
capturar la información, para ello se identifican los campos que
en cualquier caso contienen la información a ser filtrada para resumir 
el dataset."""

Tiempo_Seco = ['T_PUNTO', 'COD_25K',
            'E_PTO', 'N_PTO',
            'PH_SECO', 'COND_SECO',
            'TEMP_SECO', 'SDT_SECO',
            'RESIST_SECO', 'SAL_SECO',
            'T_NIVEL','MET_NIVEL',
            'PROF_SECO', 'PROF_AJ',
            'PROF_PZ']

Tiempo_Humed = ['T_PUNTO', 'COD_25K',
            'E_PTO', 'N_PTO',
            'PH_HUM', 'COND_HUM',
            'TEMP_HUM', 'SDT_HUM',
            'RESIST_HUM', 'SAL_HUM',
            'T_NIVEL','MET_NIVEL',
            'PROF_HUM', 'PROF_AJ',
            'PROF_PZ']
puntos = puntos[Tiempo_Humed]
# Eliminar espacios vacíos en columna Tipo de Nivel
puntos.T_NIVEL=puntos['T_NIVEL'].str.strip()
#importar y ejecutar módulo de niveles
exec(open(f'{os.getcwd()}/Nivs.py').read())

# Boxplot de datos
sbn.boxplot(y='NivelTop', x='T_NIVEL',
        data=Niveles,
        palette='colorblind')
# make grouped stripplot
sbn.stripplot(y='NivelTop', x='T_NIVEL',
                data=Niveles,
                jitter=True,
                dodge=True,
                marker='o',
                alpha=0.5,
                color='grey')
# Guardar como shapefile
exec(open(f'{os.getcwd()}/Params.py').read())
#Niveles.to_file(filename='SHP/Niveles_r2.shp',driver='ESRI Shapefile')
